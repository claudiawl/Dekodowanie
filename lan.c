#include <LPC21xx.H>
#define NULL 0

void CopyString(char pcSource[], char pcDestination[]){
	
	unsigned char ucIndeksZnaku;
	
	for(ucIndeksZnaku = 0;pcSource[ucIndeksZnaku] != NULL;ucIndeksZnaku++){
		pcDestination[ucIndeksZnaku] = pcSource[ucIndeksZnaku];
	}
	pcDestination[ucIndeksZnaku] = pcSource[ucIndeksZnaku];
}


enum CompResult{DIFFERENT, EQUAL};

enum CompResult eCompareString(char pcStr1[], char pcStr2[]){
	
	unsigned char ucIndeksZnaku;
	
	for(ucIndeksZnaku = 0;pcStr1[ucIndeksZnaku] != NULL;ucIndeksZnaku++){
		if (pcStr1[ucIndeksZnaku] != pcStr2[ucIndeksZnaku]){
			return DIFFERENT;
		}
	} //gittest
	if (pcStr1[ucIndeksZnaku] == pcStr2[ucIndeksZnaku]){
		return EQUAL;
	}
	else {
		return DIFFERENT;
	}
}


void AppendString(char pcSourceStr[], char pcDestinationStr[]){
	
	unsigned char ucIndeksZnaku;
	
	for(ucIndeksZnaku=0;pcDestinationStr[ucIndeksZnaku] != NULL;ucIndeksZnaku++){}
	CopyString(pcSourceStr, pcDestinationStr + ucIndeksZnaku);
}	


void ReplaceCharactersInString(char pcString[], char cOldChar, char cNewChar){
	
	unsigned char ucIndeksZnaku;
	
	for(ucIndeksZnaku = 0;pcString[ucIndeksZnaku] != NULL;ucIndeksZnaku++){
		if (pcString[ucIndeksZnaku] == cOldChar){
			pcString[ucIndeksZnaku] = cNewChar;
		}
	}
}

void AppendStringTest(){
	
	char cZrodlo[] = " Wlodarczyk";
	char cCel[254] = "Klaudia";
	
	AppendString(cZrodlo, cCel);	
}
/*int main(){
	
	char cZrodlo[] = " Wlodarczyk";
	char cCel[15] = "Klaudia";
	
	AppendString(cZrodlo, cCel);
}
*/

